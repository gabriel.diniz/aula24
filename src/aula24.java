public class aula24 { // precedência de operadores
    public static void main(String[] args) {


        /*
         * +
         * -
         * *
         * /
         * % - resto de uma divisão
         *
         *
         */

        int a, b, c;  //variaveis inteiras
        a = 15;  // valor
        b = 10;  // valor
        c = 5;   // valor

        int y = 0;

        y = a + b; //soma
        y = a - b; // subtração
        y = a * b; //multiplicação
        y = a / b; //divisão

        y = a % b; // resto

        // System.out.println(a * (b - c)); // vezes e sub

       // System.out.println((a + b) / a - b);

    }
}
